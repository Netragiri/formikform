import logo from './logo.svg';
import './App.css';
import FormFormik from './Form'
import HookForm from './HookForm';

function App() {
  return (
    <div className="App">
     {/* <FormFormik /> */}
     <HookForm />
    </div>
  );
}

export default App;
