import React from 'react'
import { useForm } from 'react-hook-form'
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from 'yup'



const schema = yup.object({
    firstName: yup.string().required("First Name should be required please"),
    lastname: yup.string().required(),
    email: yup.string().email().required(),
    password: yup.string().min(4).max(15).required(),
    confirmpassword: yup.string().oneOf([yup.ref("password"), null]),
  });


function HookForm() {

    const { register, handleSubmit,formState:{ errors} } = useForm({
        resolver: yupResolver(schema)
      });
    
      const submitForm = (data) => {
        console.log(data);
      };
  return (
    <div>
        <form onSubmit={handleSubmit(submitForm)}>

        Name: 
      <input type='text' name='firstName' placeholder='enter name' {...register("firstName")}></input>
      <p> {errors.firstName?.message} </p>


        Surname:
      <input type='text' name='lastname' placeholder='enter surname' {...register("lastname")}></input>
      <p> {errors.lastname?.message} </p>


    Email:
      <input type='text' name='email' placeholder='email.id' {...register("email")}></input>
      <p> {errors.email?.message} </p>


    Password:
      <input type='text' name='password' placeholder='enter password' {...register("password")}></input>
      <p> {errors.password?.message} </p>

    Confirm-password:
      <input type='text' name='confirmpassword' placeholder='confirm password' {...register("confirmpassword")}></input>
      <p> {errors.confirmpassword && "password not matches"} </p>
      <input type='submit'></input>
      </form>
    </div>
  )
}

export default HookForm
