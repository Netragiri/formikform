import React from 'react'
import {Formik,Form,Field,FieldArray} from 'formik'
import * as yup from 'yup'
import Error from './Error'

function FormFormik() {
    
    const validationSchema=yup.object({
       name: yup.string().required('Required'),
       number: yup.number()
       .required('Required')
       .min(1000000000,'enter valid number')
       .max(9999999999,'enter valid number'),
       password: yup.string().required('Required'),
       gender: yup.string().required('Required'),
       hobby: yup.string().required('Required'),
       name: yup.string().required('Required'),
    })
  return (
    <div>
      <Formik validationSchema={validationSchema} initialValues={{ 
       name:'',
       number:'',
       password:'',
       gender:'',
       hobby:'',
       address:'',
      social:[],
      hobbies: [],
      }}
      onSubmit={(items,{resetForm})=>{
        resetForm();
      }}
     >

{({ values }) => (
       <Form >
         <label>Name: </label>
        <Field name='name' type='name'/>
        <Error name='name'/>
        <br />

        <label>Phone no: </label>
        <Field name='number' type='number' />
        <Error name='number'/>
        <br /><br />

        <label>Password: </label>
        <Field name='password' type='password' />
        <Error name='password'/>
        <br /><br />

        <label>Gender: </label>
        <label>male </label>
        
        <Field name='gender' type='radio' value='male' />
        <label>female </label>
        <Field name='gender' type='radio'  value='female'/>
        <Error name='gender'/>
        <br/><br/>

        <label>Hobby: </label>
        <Field name='hobby' as='select' >
        <option value='sports'>sports</option>
        <option value='singing'>singing</option>
        <option value='dancing'>dancing</option>
        <Error name='hobby'/>
        </Field><br/><br/>


        <label>Address: </label>
        <Field name='address' as='textarea' rows='4' cols='40' /><br/><br/>

        <label>Socials</label>
            <br /> 
            <label>Instagram id:</label>
            <Field name="social[0]" type="text" />
            <br /> 
            <label>Twitter id:</label>
            <Field name="social[1]" type="text" />
            <br /> <br />
      

            <FieldArray
              name="hobbies"
              render={(arrayHelpers) => (
                <div>
                  {values.hobbies && values.hobbies.length > 0 ? (
                    values.hobbies.map((index) => (
                      <div key={index}>
                        <Field name={`hobbies.${index}`} />
                        <button
                          type="button"
                          onClick={() => arrayHelpers.remove(index)} // remove a friend from the list
                        >
                          -
                        </button>
                        <button
                          type="button"
                          onClick={() => arrayHelpers.insert(index, "")} // insert an empty string at a position
                        >
                          +
                        </button>
                      </div>
                    ))
                  ) : (
                    <button type="button" onClick={() => arrayHelpers.push("")}>
                      {/* show this when user has removed all hobbies from the list */}
                      Add a Hobbies
                    </button>
                  )}
                </div>
              )}
            />
        



        <button type='submit'>Submit</button>
       </Form>
       )}
     </Formik>
    </div>
  )
}

export default FormFormik
